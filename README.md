# pandoc-include

Include other files from your pandoc documents.

Example:

```markdown
# Book about something

!include chapter1.md

!include chapter2.md

...
```

Since pandoc-include calls pandoc internally to parse the included files, you can control additional arguments to the pandoc calls via a config file `.pandoc-include.json`

Example of `.pandoc-include.json`:

```json
{
  "args": ["-F", "pantable"]
}
```
