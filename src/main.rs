extern crate pandoc_ast;
extern crate serde;
extern crate serde_json;
extern crate rayon;
#[macro_use]
extern crate derive_error;

use std::io::Read;
use std::process::Command;
use std::path::{Path, PathBuf};

use pandoc_ast::{
    Pandoc,
    Block::{self, Para},
    Inline::{Str, Space}
};
use serde::Deserialize;
use serde_json::{
    from_str as from_json,
    to_string as to_json
};
use rayon::prelude::*;

fn main() {
    let mut input = String::new();
    let stdin = std::io::stdin();
    let mut handle = stdin.lock();
    handle.read_to_string(&mut input).unwrap();

    let mut pandoc: Pandoc = from_json(&input).unwrap();

    let args = ["-t", "json", "-o", "-", "-F", "pandoc-include"].iter().cloned();
    let user_args = user_args().unwrap_or(vec![]);
    let user_args = user_args.iter().map(|s| s.as_str());
    let args: Vec<_> = args.chain(user_args).collect();

    pandoc.blocks = pandoc.blocks.par_iter().flat_map(|b| transform(b, &args)).collect();
    println!("{}", to_json(&pandoc).unwrap());
}

fn transform(block: &Block, args: &[&str]) -> Vec<Block> {
    if let &Para(ref inlines) = block {
        if let [Str(cmd), Space, Str(filename)] = inlines.as_slice() {
            if cmd.as_str() == "!include" {
                return include(&filename, args).unwrap_or(vec![]);
            }
        }
    }
    vec![block.clone()]
}

fn include(filename: &str, args: &[&str]) -> Result<Vec<Block>, Error> {
    let filename = Path::new(filename).canonicalize()?;
    let mut args = args.to_vec();
    args.push(filename.to_str().unwrap());
    let output = Command::new("pandoc")
        .current_dir(filename.parent().unwrap())
        .args(&args)
        .output()?;
    let output = String::from_utf8(output.stdout)?;
    let pandoc: Pandoc = from_json(&output)?;
    Ok(pandoc.blocks)
}

#[derive(Debug, Error)]
enum Error {
    Io(std::io::Error),
    Utf8(std::string::FromUtf8Error),
    Serde(serde_json::Error),
    ConfigNotFound,
}

fn user_args() -> Result<Vec<String>, Error> {
    #[derive(Deserialize)]
    struct Settings {
        args: Vec<String>,
    }

    let cwd = std::env::current_dir()?;
    let file = find_file(".pandoc-include.json", cwd).ok_or(Error::ConfigNotFound)?;
    let bytes = std::fs::read(file)?;
    let str = String::from_utf8(bytes)?;
    let settings: Settings = from_json(&str)?;
    Ok(settings.args)
}

fn find_file(filename: &str, cwd: PathBuf) -> Option<PathBuf> {
    let mut file = cwd.clone();
    file.push(filename);
    if file.is_file() {
        Some(file)
    } else {
        find_file(filename, cwd.parent()?.into())
    }
}
